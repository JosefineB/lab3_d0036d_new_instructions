package controller;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import model.State;


/**
 * Lab 3
 * @author Josefine Bexelius
 * This calls handles the UDP connection and communication with its connected clients.
 */
public class InputHandler {
	
	private State gamestate;
	private int port;
	
	public InputHandler(int port, State gamestate){
		this.port = port;
		this.gamestate = gamestate;
	}
	
	/**
	 * handles the I/O of the connection
	 */
	public void runHandler(){
		try{
			DatagramSocket socket = new DatagramSocket(port);
			byte[] sendData = new byte[1024];
			byte[] recieveData = new byte[1024];
			while(true){
				DatagramPacket recievePacket = new DatagramPacket(recieveData, recieveData.length);
				socket.receive(recievePacket);
				String msg = new String(recievePacket.getData(), recievePacket.getOffset(), recievePacket.getLength());
				recievePacket.setLength(recieveData.length);
				String reply = handleMsg(msg);
				if(reply.equals("Exiting")){
					break;
				}
				sendData = reply.getBytes();
				InetAddress clientIpAddress = recievePacket.getAddress(); //extracts the ip address from datagram header.
				int clientPort = recievePacket.getPort(); //get clients port from header
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, clientIpAddress, clientPort);
				socket.send(sendPacket);
			}
			socket.close();
		}catch (Exception e){
			System.out.println(e.toString());
		}
	}
	
	/**
	 * helper method that parses input from the client
	 * @param msg : type of msg
	 * @return the servers answer based on msg type
	 */
	private String handleMsg(String msg){
		String[] msgInput = msg.split(" ");
		String reply;
		String[] colors = {"blue", "green", "yellow", "red", "orange", "purple", "black", "gray"};
		boolean validColor = false;
		for(int i = 0; i<colors.length; i++){
			if(msgInput[0].toLowerCase().equals(colors[i])){
				validColor = true;
				break;
			}
		}
		if(validColor){
			gamestate.setPosition(msgInput[0].toLowerCase(), Integer.parseInt(msgInput[1].trim()), Integer.parseInt(msgInput[2].trim()));
			reply = "Recoloring succesful";
		}
		else if(msgInput[0].toLowerCase().equals("reset")){
			gamestate.setPosition("RESET", Integer.parseInt(msgInput[1].trim()), Integer.parseInt(msgInput[2].trim()));
			reply = "Reset succesful";
		}
		else if(msgInput[0].toLowerCase().equals("exit")){
			//exit socket
			reply = "Exiting";
		}
		else{
			reply = "Invalid message";
		}
		return reply;
	}

}
