import model.State;
import view.GameGUI;


/**
 * Lab 3 D0036D
 * @author Josefine Bexelius
 *
 */
public class Main {

	public static void main(String[] args) {
		State gamestate = new State();
		GameGUI gui = new GameGUI(700, 9096, gamestate);
		gamestate.addObserver(gui); 
		gui.initGUI();

	}

}
