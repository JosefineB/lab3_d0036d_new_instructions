package model;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;


/**
 * Lab 3 D0036D
 * @author Josefine Bexelius
 * This class maintains the curremt state of the game.
 */
public class State extends Observable {

	private Color positions[][] = new Color[201][201];
	private boolean notEmpty = false;
	private Map<String,Color> colors = new HashMap<String,Color>();
	private int row;
	private int col;
	
	
	public State(){
		colors.put("black", Color.BLACK);
		colors.put("blue", Color.BLUE);
		colors.put("green", Color.GREEN);
		colors.put("red", Color.RED);
		colors.put("yellow", Color.YELLOW);
		colors.put("purple", Color.MAGENTA);
		colors.put("orange", Color.ORANGE);
		colors.put("gray", Color.GRAY);
	}
	
	/**
	 * 
	 * @return the color of the previously added position.
	 */
	public Color[][] getPositions(){
		return positions;
	}
	
	/**
	 * Handles recolorings and reset msg,
	 * updates the current rows and columns and
	 * alerts the gui.
	 * @param msg : type of msg from client
	 * @param x : which position on game board
	 * @param y : which position on game board
	 */
	public void setPosition(String msg, int x, int y){
		if(msg.equals("RESET")){
			positions[x][y] = Color.WHITE;
		}
		else{
			positions[x][y] = getColor(msg);
		}
		row = x;
		col = y;
		notEmpty = true;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * 
	 * @return recently updated x position
	 */
	public int getRow(){
		return row;
	}
	
	/**
	 * 
	 * @return recentöy updated y position
	 */
	public int getCol(){
		return col;
	}
	
	/**
	 * @return true if there is something added on one of the game boards positions, else false
	 */
	public boolean isNotEmpty(){
		return notEmpty;
	}
	
	/**
	 * @param msg : string representation of requested color
	 * @return requested color
	 */
	private Color getColor(String msg){
		return colors.get(msg);
	}
	
}
