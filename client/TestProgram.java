package client;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;


/**
 * Lab 3 D0036D
 * @author Josefine Bexelius
 * Simple client class used to test the server
 */
public class TestProgram {

	public static void main(String[] args) {
		int port = 9096;
		String addr = "localhost";
		try{
			DatagramSocket clientSocket = new DatagramSocket(0);
			byte[] sendData = new byte[1024];
			byte[] recieveData = new byte[1024]; 
			InetAddress serverAddress = InetAddress.getByName(addr);
		    Scanner in = new Scanner(System.in);  
		    while(true){
			   	System.out.println("Write your input in this format -> type posx posy \nType is either a chosen color or reset. \nPostions ranges from 0-200 \nType exit to exit");
		   		String stringSendData = in.nextLine();
		   		if(stringSendData.toLowerCase().equals("exit")){
		   			break;
		   		}
				sendData = stringSendData.getBytes();
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, serverAddress, port);
				clientSocket.send(sendPacket);
				DatagramPacket rececivePacket = new DatagramPacket(recieveData, recieveData.length);
				clientSocket.receive(rececivePacket); //The client waits here until it recieves packets, when that happens that packet is put into recievePacket.
				String msg = new String(rececivePacket.getData(), rececivePacket.getOffset(), rececivePacket.getLength());
				rececivePacket.setLength(recieveData.length);
				System.out.println("FROM SERVER: " + msg);
		    }
			clientSocket.close();
			
		}catch (Exception e){
			System.out.println(e.toString()); //Throws socket exception
		}

	}

}
