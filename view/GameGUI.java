package view;

import java.util.Observable;
import java.util.Observer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.InputHandler;
import model.State;

/**
 * Lab 3 D0036D
 * @author Josefine Bexelius
 * This class handles the graphical representation of the game's state.
 */
public class GameGUI implements Observer {
	
	private int size;
	private int serverport;
	private State gamestate;
	private InputHandler input; 
	
	private JFrame window;
	private GamePanel panel;
	
	/**
	 * 
	 * @param size :  the size of the game board
	 * @param port : what the server listens on
	 * @param state : instance of the current state of the game
	 */
	public GameGUI(int size, int port, State state){
		this.size = size;
		this.serverport = port;
		this.gamestate = state;
		this.input = new InputHandler(serverport, gamestate);
	}
	
	/**
	 * Sets up and presents the gui layout
	 */
	public void initGUI(){
		window = new JFrame("GUI");
		panel = new GamePanel(size, gamestate);
		window.setContentPane(panel);
		window.setSize(size, size);
		window.setVisible(true);
		window.setResizable(true);
		window.setBackground(Color.WHITE);
		input.runHandler();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
		
	
	
	@Override
	public void update(Observable arg0, Object arg1) {
		// Every time state changes redraw the game board
		panel.repaint();
	}

}
