package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import model.State;

/**
 * Lab 3 D0036D
 * @author Josefine Bexelius
 * this class handles the drawing of the game content
 */
public class GamePanel extends JPanel {

	private int size;
	private State gamestate;
	
	
	public GamePanel(int size, State gamestate){
		this.size = size;
		this.gamestate = gamestate;
	}
	
	@Override
	public void paint(Graphics g) {
		Color[][] positions = gamestate.getPositions();
		if(gamestate.isNotEmpty()){
			int x, y; 
			x = gamestate.getRow() * 20; 
			// Set y coordinates of rectangle 
			// by 20 times 
			y = gamestate.getCol() * 20;
			g.setColor(positions[gamestate.getRow()][gamestate.getCol()]);	  
			// Create a rectangle with 
			// length and breadth of 20 
			g.fillRect(x, y, 20, 20); 
		}
	} 
	
}
